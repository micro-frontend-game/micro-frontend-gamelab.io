"use strict";

import loadComponent from '/loader/loader.mjs'
import busBuilder from '/bus/bus.mjs'

const bus = busBuilder()

;(async ()=> {
  const Notifier = await loadComponent('/notifier/notifier.mjs', { bus })
  const TableTop = await loadComponent('/table-top/TableTop.mjs', { bus })
  console.log('Notifier:', Notifier, Notifier.tagName)
  TableTop.create({ parent: document.body })
  const notifier = Notifier.create({ parent: document.body })
  const p = document.createElement('p')
  p.innerText = 'This is a childrem element test.'
  notifier.appendChild(p)
})()
.catch(err => {
  console.error(err)
  alert('Ups... Uncatch error.\n\n' + err.message)
})
