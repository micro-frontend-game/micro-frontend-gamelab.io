Micro Front-End Main
====================

This is the application root launcher of a dependency-less experiment of Micro Front-End.
This is a memory game, with each component implemented on a separated repo inside this GitLab group and deployed separately.

Try it at https://micro-frontend-game.gitlab.io

Development
-----------

### Cloning

Create a `micro-frontend-game` directory on your computer and inside that clone all repositories that you can find here: https://gitlab.com/micro-frontend-game (do not change the dir name).


### Running

In a terminal run:
```bash
cd micro-frontend-game/micro-frontend-game.gitlab.io
python3 -m http.server 3000
```


**FAQ**
-------

### What I need to run it locally?

* Git to clone the repos in this group;
* Python3 or another simple file web server;
* A browser to run the resulting SPA.

This is a dependency-less experiment, so you don't need to rum `npm install` or something alike.

### Why you have symlink to the other repository names on this repo root?

This symbolic links mimics the published structure on GitLab pages. So if you clone as we told you to do at **Development** > **Cloning** a simple local file server like `python3 -m http.server 3000`, inside this repo root, will enable you to get the same result as deploying it all.
